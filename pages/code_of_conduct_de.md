---
layout: page
title: Verhaltenskodex
permalink: /code_of_conduct_de/
hide: true
---
[ENGLISH](https://tzm.community/code_of_conduct/) ||| [ÍSLENSK](https://tzm.community/code_of_conduct_is/) ||| [SVENSKA](https://tzm.community/code_of_conduct_sv/)

## **LIEBES MITGLIED DER MENSCHENFAMILIE**

Wir begrüßen Dich bei der Zeitgeist-Bewegung (eng. “The Zeitgeist Movement”; im Folgenden auch “TZM”), einer weltweiten Graswurzelbewegung, die sich für Nachhaltigkeit und globale soziale Veränderung einsetzt. Die Teilnahme eines jeden Einzelnen ist notwendig, um am größten sozialen Wandel der Welt mitzuwirken, der jemals in der Geschichte begonnen hat. Das Ziel dieser Bewegung ist es, Licht auf eine neue Sichtweise zu werfen, einen neuen Gedankengang zu entwickeln und die wichtigsten Erkenntnisse über ein nachhaltigeres und harmonischeres Leben miteinander und mit der übrigen Natur in Einklang zu bringen.

Deine Beteiligung an dieser Bewegung ermöglicht Dir, Dich in vielfältigen Bereichen zu engangieren, wie z.B. gemeinsame regionale und überregionale Treffen, wissenschaftliche und technologische Forschungen und Projekte, lokale und globale Kooperationen, Wohltätigkeitsaktionen, Pflege und Schutz der Umwelt und sogar persönliche Entwicklung.Jeder von uns ist persönlich für die Einhaltung unserer Grundwerte mit verantwortlich, die die Wahrung eines ethischen Verhaltens erfordern.

Wir haben diesen Verhaltenskodex erstellt, um unser langjähriges Engagement für die Einhaltung dieser Verantwortung zu bekräftigen und um eine Anleitung für Deine und jedermanns Teilnahme an dieser Bewegung zu geben.Auf unserem weiteren Weg will die Zeitgeist-Bewegung dazu beitragen, dass die Botschaft eines nachhaltigeren, kooperativen und integrativen Zusammenhalts in der Gesellschaft Gehör findet. Eine Verpflichtung zur Integrität, zum ehrlichen und ethischen Handeln und zur Einhaltung der globalen Menschenrechte sind entscheidend für unseren weiteren Erfolg.

Wir danken Dir für Deine Teilnahme.

## **KURZE ZUSAMMENFASSUNG**

**1. Bitte behandle jeden mit Respekt**

> Beleidigende, abwertende und/oder sonstige respektlose Äußerungen sind unangebracht und inakzeptabel.

**2. Bitte lass Dich nicht auf persönliche Angriffe (Ad Hominem) ein**

> “Ad Hominem” bedeutet einen verbalen Angriff auf die Person, anstatt sich mit ihren Argumenten oder dem Thema auseinanderzusetzen. Versuche nicht auf solche Angriffe zu reagieren, sondern die Situation höflich zu entschärfen und die Diskussion wieder auf das Thema zu lenken.Drohungen sind nicht erlaubt, egal ob gegenüber Einzelpersonen, Gruppen oder der Gemeinschaft als Ganzes. Drohungen sind respektlos und völlig gegen den Gedankengang von TZM.

**3. Bringe Dich konstruktiv in laufende Aktivitäten ein und störe sie nicht**

> Bitte respektiere die jeweilige Zielsetzung von aktuellen Projekten, Aufgaben, Versammlungen oder Zusammenkünften jeglicher Art. Während Du grundsätzlich an allen Aktivitäten der Bewegung teilnehmen kannst, solltest Du die jeweiligen Abläufe nicht stören, TeilnehmerInnen ablenken oder Dich respektlos verhalten.

**4. Beachte den Verhaltenskodex und halte ihn ein**

> Es liegt in Deiner Verantwortung, den Verhaltenskodex einzuhalten. Informiere auch andere TeilnehmerInnen darüber, um Dich selbst, andere und die Integrität von TZM zu schützen. Jeder ist aufgefordert, dies zu berücksichtigen.

**5. Verwandle Unterschiede in Stärken**

> Konzentriere Dich darauf, Probleme zu lösen und aus Fehlern zu lernen, anstatt Dich an Unstimmigkeiten aufzuhalten. Suche nach Gemeinsamkeiten statt nach Unterschieden - Kollaboration statt Konkurrenz.

## **ETHIKKODEX & GEMEINSCHAFT**

Die Zeitgeist-Bewegung praktiziert Gleichberechtigung, Transparenz und Chancengleichheit für jeden, der einen Beitrag zur Bewegung leisten möchte. Wenn eine oder mehrere Personen im Namen von TZM aktiv werden möchten, müssen sie den hier beschriebenen Verhaltenskodex einhalten. Das Ziel ist es, ein gemeinschaftliches Umfeld in Verbindung mit TZM zu etablieren, so dass die Bildung, Gesundheit, Sicherheit und geistige sowie körperliche Entwicklung einer Person nicht behindert wird. Der Verstoß gegen den Verhaltenskodex im gemeinschafltichen Umfeld oder im Namen von TZM kann zum Ausschluss und zur Verweigerung der Verbindung mit TZM sowie ggf. zu rechtlichen Schritten führen.

## **WIE WIR UNSER GEGENÜBER BEHANDELN SOLLTEN**

Innerhalb von TZM behandeln wir uns gegenseitig mit Respekt und Würde. Das bedeutet, dass alle Personen das Recht haben, an der Bewegung teilzunehmen, und sich in einem gemeinschaftlichen Umfeld frei von Belästigung, Mobbing und Diskriminierung bewegen zu können.Belästigung, Mobbing und Diskriminierung können sich vielfältig äußern, z. B.:
* Unerwünschte Bemerkungen, Gesten oder Körperkontakt
* Das Zeigen oder Verbreiten von beleidigenden, abwertenden oder sexuell eindeutigen Bildern oder anderen Materialien
* Beleidigende oder abwertende Witze oder Kommentare (explizit oder durch Anspielungen)  
* Verbaler oder körperlicher Missbrauch oder Drohungen
* Wut oder Frustration an anderen auslassen

Diese Bewegung ist nicht hierarchisch und lehnt sämtliche Lösungen ab, die Gewalt, Zwang oder Nötigung einsetzen, um Frieden und Harmonie zu erreichen. Sollte jemand diesem zuwiderhandeln wird ihm die Verbindung zu TZM verweigert.

## **INKLUSION - FÜHLE DICH INTEGRIERT**

Wir begrüßen und unterstützen Menschen aller Hintergründe und Identitäten. Alle Personen jeglicher sexueller Orientierung, Geschlechtsidentität und -ausdruck, Ethnie, Kultur, nationaler Herkunft, sozialem und wirtschaftlichen Status, Bildungsniveau, Hautfarbe, Migrationshintergrund, Geschlecht, Alter, körperlicher Verfassung, Familienstand, politischer Überzeugung, religiösem Glauben sowie geistiger und körperlicher Fähigkeiten sind willkommen. Es ist keine Mitgliedschaft, Gebühr, akademischer Hintergrund, besondere Qualifikation, Gelöbnis oder irgendetwas anderes außer der Zustimmung zu diesem Verhaltenskodex für die Teilnahme erforderlich. Es ist von zentraler Wichtigkeit für TZM, die Integrität, den Ruf und die Sicherheit aller Personen zu schützen, die mit der Bewegung verbunden sind, sowie die eigene.

Jeder kommt mit einem anderen Hintergrund, anderen Lebenssituationen und anderen Vorgeschichten. Wir sind uns bewusst, dass Menschen in der Lage sind, Dinge zu sagen oder zu tun, die nicht beabsichtigt sind. Trotz gelegentlicher Fehltritte verstehen wir, diese als Teil eines größeren (Lern-)Prozesses mit einzubeziehen. Falls Du vorübergehend nicht in der Lage sein solltest, den Verhaltenskodex von TZM einzuhalten, schlagen wir Dir vor, eine Pause einzulegen und zu einem späteren Zeitpunkt wieder einzusteigen. Wenn Du allerdings nicht in der Lage sein solltest, Dich an den Verhaltenskodex zu halten, empfehlen wir Dir, Dich von der Bewegung zu trennen.

## **VIELFALT - FÜHLE DICH RESPEKTIERT**

Wir können Stärke in der Vielfalt finden. Verschiedene Menschen haben unterschiedliche Sichtweisen auf Themen, und das kann wertvoll sein, um Probleme zu lösen oder neue Ideen zu entwickeln. Wenn Du nicht verstehen kannst, warum jemand einen bestimmten Standpunkt vertritt, bedeutet das nicht, dass die Person falsch liegt. Vergiss nicht: Wir machen alle Fehler und gegenseitige Schuldzuweisungen bringen uns nicht weiter!Konzentriere Dich stattdessen darauf, Probleme zu lösen, aus Fehlern zu lernen und neue Standpunkte zu verstehen. 

## **GELTUNGSBEREICH DES VERHALTENKODEXES**

Dieser Vehaltenskodex gilt für jede Zusammenkunft, jedes Gemeindezentrum, jede Veranstaltung, jedes Projektumfeld, jede Website oder andere Druckmedien, soziale Medien, Instant-Messaging-Gruppen sowie jedes weitere aktivistische Umfeld, das mit TZM in Verbindung steht. Es wird empfohlen, dass ein Exemplar des Verhaltenskodexes für alle sichtbar in den Gemeinschaftsbereichen zur Verfügung gestellt wird. Dies soll den Schutz, die Integrität, die körperliche wie geistige Unversehrtheit und die Inklusion aller Personen sicherstellen, die sich in einem Gemeinschaftsumfeld von TZM aufhalten.

Es wird von allen mit TZM in Verbindung stehenden Personen erwartet, dass sie den Verhaltenskodex der Bewegung und alle zusätzlichen Sicherheitsvorkehrungen beachten und umsetzen und alle anderen Personen, die mit TZM in Verbindung stehen, daran erinnern, da die Verantwortung nicht nur bei den Personen liegt, die an der Gemeinschaft beteiligt sind.Eine Missachtung des Verhaltenskodex kann sanktioniert werden. Sollte dies keine Wirkung entfalten, kann das zum Ausschluss aus TZM führen.

## **QUALITÄTSSTANDARDS FÜR INHALTE & AKTIVISMUS**

Jede teilnehmende Person ist willkommen, für TZM Inhalte zu erstellen oder sich aktivistisch zu beteiligen, mit der Absicht die bestmögliche Integrität der Bewegung aufrechtzuerhalten. Dafür ist es unbedingt erforderlich, stets den Verhaltenskodex zu befolgen.Dieser Standard gilt in allen Darstellungs- und Ausdrucksformen, Sprachen oder Gesten, mit denen TZM nach aussen repräsentiert wird.

Wenn Du im Namen der Bewegung Aktivitäten unternimmst oder Inhalte erstellst und Dich mit TZM und allen mit ihr verbundenen Personen assoziierst, stimmst Du dem Verhaltenskodex von TZM und der Verantwortung zu, diesen einzuhalten und andere über seine Grundsätze zu informieren. Obwohl Du für die Aufnahme von Aktivitäten innerhalb von TZM keine Qualifikationen oder Nachweise benötigst, gilt es einem Qualitätsstandard zu folgen, der die Bewegung und die beteiligten Personen angemessen repräsentiert. Wer an der Zeitgeist-Bewegung teilnehmen möchte, aber die Grundlagen ihres Aktivismus' und ihrer Inhalte nicht kennt, sollte das Buch “The Zeitgeist Movement: Defined” lesen, das unter “Links & Ressourcen” zu finden ist.

Die wesentlichsten Aktivitäts- und Inhaltsschwerpunkte von TZM beinhalten philosophische, bewährte und hypothetische Methoden der Nachhaltigkeit, Wissenschaft, Technologie, Menschenrechte, Umweltschutz und Wohltätigkeitsaktionen zur Unterstützung und Verbesserung der globalen Gesellschaft aller Menschen und der Natur gleichermaßen.

Es gibt einige Bereiche, die als schädlich oder als falsche Darstellung von TZM und denjenigen, die mit TZM in Verbindung stehen, angesehen werden können. Verfälschungen der Aktivitäten oder Gedankengänge von TZM werden nicht toleriert, ebenso wenig wie irreführende Informationen, die behaupten, mit der Bewegung übereinzustimmen. Dies schließt ein, ist aber nicht beschränkt auf:

* die Rechtfertigung der Anwendung von Gewalt, Zwang, Manipulation, Zerstörung oder Kontrolle gegen andere, um Ziele zu erreichen
* die Verwendung oder Rechtfertigung von Hassreden oder Diskriminierung gegen Menschen und/oder andere Gruppen, politische Parteien oder Nationen
* die Fälschung von Informationen für eine Agenda, ob zur Unterstützung oder Ablehnung von TZM
* die Förderung von politischen Parteien oder Kampagnen zur Förderung von TZM
* das Handeln oder die Registrierung von TZM als politische Einheit oder religiöse Institution
* die Verwendung von falschen Vorstellungen, Annahmen oder Informationen ohne substanzielle Beweise zur Unterstützung einer Behauptung

Sollte eine Person, die mit TZM verbunden ist, absichtlich einen Inhalt oder eine Aktivität mit einem der genannten Merkmale wiedergeben, wird sie aus der Bewegung ausgeschlossen und darf nicht weiter an Aktivitäten innerhalb von TZM teilnehmen. Allen Gruppierungen, ganz gleich ob sie eine Verbindung zu TZM haben oder nicht, und die eines der vorherigen Merkmale verwenden, um die Motivationen und Ambitionen der Bewegung zu beschreiben, wird durch eine Erklärung widersprochen und sie müssen mit rechtlichen Schritten rechnen.

Wähle Deine Worte sorgfältig, verhalte Dich möglichst/immer professionell, sei freundlich zu anderen und zögere nicht um Hilfe zu bitten, wenn Du Unterstützung zur Realisierung eines Projektes innerhalb/für der/die Bewegung brauchst.

## **FINANZIELLE ANGELEGENHEITEN**

TZM arbeitet nicht gewinnorientiert. Jegliches im Namen von TZM gesammelte Geld wird ausschließlich zur Finanzierung oder Rückerstattung von Aktivitäten der Bewegung verwendet. Dies gilt für alle in Bezug zu TZM gegründeten gemeinnützigen Organisationen, Nichtregierungsorganisationen und andere finanzielle / rechtliche Organisationseinheiten sowie im Zusamenhang damit genutzte Anleihen, Fonds und Bankkonten. Finanzielle und juristische Organisationen, die mit TZM in Verbindung stehen, dürfen keinen Profit für Dritte erwirtschaften, egal ob diese mit der Bewegung in Verbindung stehen oder nicht.

Sollte etwas derartiges passieren, werden die betreffenden Organisationen oder Personen aus der Bewegung ausgeschlossen und dürfen sich nicht weiter an Aktivitäten innerhalb von TZM beteiligen. Die Bewegung wird sich hiervon distanzieren und behält sich vor, ggf. rechtliche Schritte einzuleiten.

## **HABT SPAß!**

Es geht nicht nur darum, das finale Ziel zu erreichen, sondern auch um die Reise - um die vielen Menschen, denen Du auf dem Weg begegnest, um die Dinge, die Du dabei lernst und um die Schritte und Meilensteine, die Du erreichst. Lass Deine Aktivitäten und Dein Wirken für die Zeitgeist-Bewegung nicht nur eine lästige Pflicht oder eine langweilige Aufgabe sein, die erledigt werden muss. Versuche ebenso ein nachhaltigeres, gesünderes und erfüllteres Leben zu führen. Sei freundlich zu Deinen Nachbarn und Deiner Familie, hilf Deinem näheren Umfeld und fördere eine Zukunft für alle ohne Armut und Gewalt, während Du auch das große Ganze im Blick behältst.

Wir freuen uns auf Dich und Deine unterstützenden Beiträge in der Zeitgeist-Bewegung und für die gesamte Menschheit. Werde die Veränderung, die Du in der Welt sehen möchtest!

## **KONTAKT**

Die Zeitgeist Bewegung Globale Webseite – https://www.thezeitgeistmovement.com

Die Zeitgeist Bewegung Community Webseite- https://tzm.community/ 

## **LINKS & REFERENZEN**

The Zeitgeist Movement: Defined – https://www.thezeitgeistmovement.com/wp-content/uploads/2017/10/The_Zeitgeist_Movement_Defined_PDF_Final.pdf

The Zeitgeist Movement: Activist Guide – https://tzm.community/activism/

#### **Version 1.0** Veröffentlicht 27th Nov 2020
