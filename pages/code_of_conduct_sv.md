---
layout: page
title: Uppförandekod
permalink: /code_of_conduct_sv/
hide: true
---
[DEUTSCH](https://tzm.community/code_of_conduct_de/) ||| [ENGLISH](https://tzm.community/code_of_conduct/) ||| [ÍSLENSK](https://tzm.community/code_of_conduct_is/)

## **KÄRA VÄRLDSMEDBORGARE**

Vi välkomnar dig till The Zeitgeist Movement, en världsomspännande och hållbarhetsfrämjande rörelse som verkar för social förändring på ett globalt plan. Ditt och andras deltagande krävs för att tillsammans skapa världens största sociala förändring som någonsin påbörjats i historien. Målet med denna rörelse är att främja kunskapen och att ge ett nytt perspektiv om ett hållbart levnadssätt, där människor lever i harmoni med varandra och resten av naturen.

Genom att delta i denna rörelse verkar du i sociala sammanhang som till exempel vetenskaplig och teknisk forskning och praxis, lokala och globala samhällsinsatser, välgörenhetsoperationer, miljöräddning och även personlig utveckling.

Var och en av oss är personligen ansvarig för att stödja våra kärnvärden, vilket kräver att en etisk uppförandekod följs. Vi har skapat denna uppförandekod för att upprätta och säkerställa vårt långvariga åtagande och för att ge vägledning för din och allas deltagande i rörelsen.

Zeitgeist-rörelsen kommer att främja budskapet om behovet av ett mer hållbart, samverkande och inkluderande samhälle. Ett åtagande från samtliga deltagare att handla med integritet, att handla ärligt och etiskt, samt att följa lagen, är avgörande för vår fortsatta framgång.

Tack för din medverkan.

## **KORT SAMMANFATTNING**

**1. Behandla alla med respekt.**

> Förolämpande, nedsättande och/eller annan respektlös handling är olämplig och oacceptabel. 

**2. Vänligen delta inte i personangrepp/Ad Hominem-argument.**

> Ad Hominem-argument innebär att en enskild persons egenskaper diskuteras och/eller attackeras i stället för att kärnämnet behandlas. Försök att inte svara på sådana attacker, förutom att artigt påvisa situationen och omdirigera diskussionen till ämnet.

Hot är inte tillåtna, varken mot individer, grupper, eller samhället som helhet. Hot är respektlöst och står i strid med TZM:s värdegrund.

**3. Respektera aktivitetens specifika uppgift.**

> Delta med rätt avsikter när du stödjer ett visst projekt, uppgift, eller något annat moment som utförs av rörelsen. Även om aktiviteten är inkluderande för alla, bör du inte belasta, distrahera, vara respektlös eller störande under den aktiviteten.

**4. Kom ihåg att upprätthålla uppförandekoden.**

> Det finns ett gemensamt ansvar i att upprätthålla och informera samtliga som deltar i TZM kring vår uppförandekod. Detta ansvar krävs för att skydda dig, andra deltagare och själva integriteten inom TZM. Det kan inte göras på egen hand av en enskild deltagare och därför har alla inom rörelsen ett gemensamt ansvar för att säkerställa detta.

**5. Hitta styrkor i varandras olikheter.**

> Fokusera på att lösa problem och att lära av misstag istället för att försöka förändra en annan persons värderingar/egenskaper. Gör samhället större och mer inkluderande. Samarbeta istället för att konkurrera.

## **ETISK KOD & SAMVERKANSMILJÖ**

The Zeitgeist Movement (TZM) bygger på jämlikhet, transparens och lika möjligheter för alla som vill bidra till rörelsen. När personer hävdar att de associeras med TZM och påbörjar aktivitet i samverkan med andra personer, måste de följa uppförandekoden enligt instruktionerna i detta dokument. Målet är att skapa en samverkansmiljö inom TZM som inte hämmar en persons hälsa, säkerhet och/eller mentala eller fysisk utveckling. Underlåtenhet att följa denna uppförandekod, särskilt vid samverkan eller när man uttryckligen hävdar associering med TZM, kan leda till uteslutning och förnekande av associering med TZM, samt vid behov rättsliga åtgärder.


## **HUR VI BEHANDLAR ANDRA**

Inom TZM behandlar vi varandra med respekt och värdighet. Detta innebär att alla personer har rätt att delta i rörelsen, i en miljö utan trakasserier, mobbning och diskriminering.

Trakasserier, mobbning och diskriminering inkluderar bland annat:
* Oönskade kommentarer, gester eller fysisk kontakt
* Visning eller cirkulation av stötande, nedsättande eller sexuellt uttryckliga bilder eller annat stötande material
* Stötande eller nedsättande skämt eller kommentarer (uttryckliga eller implicita) 
* Verbal eller fysiskt missbruk eller hot
* Uppvisa ilska eller frustration mot andra

Denna rörelse är icke-hierarkisk och avvisar alla föreslagna lösningar som använder våld eller tvång. Målet är att uppnå fredliga och harmoniska lösningar. Om någon hävdar det motsatta av detta uttalande när de gör anspråk på association till TZM kommer association att nekas.

## **ATT VARA INKLUDERANDE**

Vi välkomnar och stöder människor med alla bakgrunder och identiteter. Detta innebär att vi välkomnar personer oavsett personens sexuella läggning, könsidentitet och uttryck, etnicitet, kultur, nationella ursprung, social och ekonomisk klass, utbildningsnivå, hudfärg, invandringsstatus, kön, ålder, storlek, familjestatus, politisk tro, religion och mental och fysisk förmåga. Samtliga är välkomna att bidra till rörelsen förutsatt att uppförandekoden följs.

Förutom själva kravet att följa uppförandekoden finns det inga krav. Det utgår således inga krav på medlemskapsavgift, studieavgift, akademisk bakgrund, specifika referenser, pant eller annat för att delta.

Det är TZM:s uttryckliga avsikt att skydda integriteten, anseendet och säkerheten för alla personer som är förknippade med rörelsen, vilket även gäller för rörelsen som helhet.

Alla kommer från olika bakgrund, med olika utmaningar och olika historier. Vi förstår att människor kan säga och göra saker som inte avsågs från början. Trots eventuella misstag är målet att vi alltid försöker inkludera samtliga som en del i helheten. Om du eller någon person inte kan upprätthålla TZM:s uppförandekod vid något enstaka tillfälle, föreslås det att du tar en paus och kommer tillbaka vid en bättre lämpad tidpunkt för att delta. Om du är helt oförmögen att agera inom uppförandekoden rekommenderar vi att du avsäger dig kontakt med rörelsen.

## **MÅNGFALD**

Vi kan hitta en styrka i mångfalden. Olika människor har olika perspektiv på frågor, och det kan vara värdefullt för att lösa problem eller att generera nya idéer. En annan persons synvinkel betyder inte att de har fel. Glöm inte att vi alla gör misstag. Att skylla på varandra leder inte någonstans.

Fokusera istället på att lösa problem, att lära av misstag och att förstå nya synpunkter.

## **GEMENSKAPS MILJÖT**

Alla mötesplatser, gemenskapscenter, evenemang, projektplatser, hemsidor eller annan media, social-media, direkt-meddelande grupper och alla aktivist utföranden som är förknippade med TZM defineras som gemenskaps miljöer inom rörelsen. Alla gemenskaps miljöer förknippade med TZM förväntas följa rörelsens uppförandekod. Det rekomenderas även att en kopia av uppförandekoden är tillgänglig för alla att se inom gemenskaps miljöerna. Detta omfattar att försäkra säkerheten, integriteten, fysiska säkerheten, psykiska hälsan och inklusiviteten för alla närvarande i en TZM gemenskaps miljö.
Personer som förknippar sig med TZM förväntas följa uppförandekoden och yttligare säkerhets regler och sätt passande till situationen under en gemenskaps miljö. Alla förväntas upprätthålla och påminna andra delaktiga i förknippelse med TZM om rörelsens uppförandekod, såväl som yttligare säkerhetsårgärder, eftersom ansvaret inte ligger i ett begränsat antal personer delaktiga i gemenskaps miljön.

Om någon gemenskaps miljö misslyckas att uppfylla uppförandekodens krav och motåtgärder inte blivit uträttade så kommer denna gemenskaps miljö att bli borttagen och sluta vara förknippad med TZM.


## **KVALITETSSTANDARD FÖR MATERIAL & AKTIVISM**

Alla delaktiga i verksamhet för TZM är välkomna att skapa material eller engagera sig i stöd av aktivitet i syfte att upphålla rörelsens bästa helhet tillsammans med alla som hävdar sig förknippelse med samt samtycker till rörelsens uppförelsekod. Detta krav tillämpas till allting, på alla sätt, på alla språk och i alla gester skildrade att representera TZM eller annan person förknippad med TZM. 

Vid genomförande av aktivitet eller skapande av material inom rörelsen och du förknippar dig själv med TZM och alla andra förknippade personer, samtycker du till TZM:s uppförandekod och ansvaret att upprätthålla och upplysa andra om dess principer. Trots att det inte finns några krav för tidigare kvalifikationer för att påbörja verksamhet inom TZM så finns det krav att följa en hög standard som ordentligt framställer rörelsen och personerna delaktiga. För dem som önskar att delta och påbörja verksamhet inom TZM men är samtidigt osäkra på grunden till dess aktivism och material hänvisas läsa boken "The Zeitgeist Movement: Defined," vilket ligger under "LÄNKAR & REFERENSER."

Några av de huvudsakliga fokusen för verksamhet och material inom TZM omfattas av de filosofiska, hypotetiska och bästa sätten för hållbarhet, vetenskap, teknologi, mänskliga rättigheter, miljöskydd och välgörenhetsverksamhet åt ett stödjande skäl och förbättringen av det världsomspännande samhälle för människa så som skog. Allt det som inte framgår enligt tidigare skrivning anses som orelevant eller av låg relevans för TZM.

Det finns även ämnen som inte bara anses som orelevanta, utan skadliga eller som oriktig framställning av TZM och dess anhängare. Felaktig representation av TZM:s verksamhet eller tankestpår kommer inte att tolereras eller vilseledande uppgifter som hävdar sig stämma med rörelsen. Detta omfattar men är begränsas inte till: rättfärdigande till bruk av våld, tvång, manipulation, skadogörelse eller kontroll mot andra för att uppnå mål; användning eller rättfärdigande till bruket av hets eller kränkning emot folkgrupper och eller andra grupper, politiska partier eller nationer; vanställning av uppgifter för en agenda vare sig i anslutning till eller i missnöje av TZM; främja politiska partier eller kampanjer i främjande av TZM; verka eller registrera TZM som någon politiskt enhet; användningen av misstydning, antagande eller uppgifter utan ansevärt bevis i främjande av ett påstående. 

Skulle någon i förknippelse med TZM framställa något material eller verksamhet med någon av dessa angivna kännetecken världsomspännande, skulle deras medverkan därav exkluderas från rörelsen och kommer inte tillåtas att förknippa sig yttligare inom TZM:s verksamhet. Grupper som ej hävdar sig förknippelse med TZM och använder sig av något kännetecken för att beskriva rörelsens motiveringar och ambitioner kommer att vederläggas med en följande redogörelse och kan komma att stå inför rättsliga åtgärder.

Välj dina ord noga, framträd alltid professionellt, var vänlig mot andra och tveka inte att fråga om hjälp om du behöver medhjälp att göra något betydelsefullt inom rörelsen.

## **FÖRMÖGENHETSFÖRHÅLLANDEN**

TZM fungerar genom en icke-kommerciell sedvänja och har därför inget vinstintresse. Penning som samlas in i TZM:s namn är enbart använt för att finansiera eller ersätta ett utlägg för rörelsens verksamhet. Alla icke-vinstdrivande organisation status, icke-statlig status, obligation, fond, bankkonto eller annan finansiell / juridiska organisationsorgan som använder eller är förknippad med TZM förväntas att följa detta. Dessa finansiella / juridiska organisationsorgan i förknippelse med TZM avstås från att skapa vinst åt någon person vare sig förknippad eller icke-förknippad med rörelsen. Skulle detta förekomma, skulle det finansiella / juridiska organisationsorganet, personen i förknippelse eller icke-förknippelse med TZM, att bli exkluderad från rörelsen och kommer inte tillåtas att förknippa sig yttligare inom TZM:s verksamhet, deras handlingar vederläggs med en följande redogörelse och kan komma att stå inför rättsliga åtgärder.

## **HA SKOJ!**

Det handlar inte bara om att nå ändamålet, utan också att njuta av resan längs vägen. Alla andra du träffar på, det du lär sig, stegen och milstolparna du når. Låt inte dina aktiviteter och arbete för Zeitgeist-rörelsen bara vara sysslor eller trista uppgifter som måste utföras. Försök istället leva ett mer hållbart, hälsosamt och fullständigt liv. Var vänlig mot dina grannar och din familj, hjälp din lokala gemenskap och så en framtid för alla, utan fattigdom och våld, under tiden som du ser den större bilden.

Vi ser framemot att träffa dig och dina stöttande bidrag till TZM and resten av männskligheten och naturen. Vare förändringen du önskar dig att se i världen.

## **KONTAKT**

Zeitgeist-rörelsens hemsida – – https://www.thezeitgeistmovement.com

Zeigeist-rörelsens gemenskaps hemsida- https://tzm.community/ 

## **LÄNKAR & REFERENSER**

The Zeitgeist Movement: Defined – https://www.thezeitgeistmovement.com/wp-content/uploads/2017/10/The_Zeitgeist_Movement_Defined_PDF_Final.pdf

The Zeitgeist Movement: Activist Guide – https://tzm.community/activism/

#### **Version 1.0** Released 27th Nov 2020
