---
layout: page
title: Code of Conduct
permalink: /code_of_conduct/
---
[DEUTSCH](https://tzm.community/code_of_conduct_de/) ||| [ÍSLENSK](https://tzm.community/code_of_conduct_is/) ||| [SVENSKA](https://tzm.community/code_of_conduct_sv/)

## **DEAR FELLOW CITIZEN OF THE WORLD**

We welcome you to The Zeitgeist Movement, a worldwide grassroots, sustainability advocacy organization for global social change. Your participation is needed to be a part of the world’s largest social change. It is the goal of this movement to bring to light a new perspective and way of thinking, as well as bringing the best knowledge of living more sustainably, and in harmony with each other and the rest of nature.
By participating in this movement, you will be involved in social assemblies, scientific and technological research and practices, local and global community efforts, charity operations, environmental rescue and even personal development.
Each of us is personally responsible for supporting our core values, which require compliance with an ethical conduct. We have issued this Code of Conduct to restate our longstanding commitment to uphold that responsibility, and to provide guidance to your participation in the movement.
As we move forward, The Zeitgeist Movement (TZM) will help ensure that the message of a more sustainable, collaborative and inclusive coherence in society is made heard. A commitment to integrity, acting honestly and ethically, and complying with international human rights laws are critical to our continued success.

Thank you for your participation.

## **SHORT SUMMARY**

**1. Please treat everyone with respect.**

> Insulting, derogatory and/or any other disrespectful discourse is inappropriate and unacceptable. 

**2. Please do not engage in personal attacks/Ad Hominem.**

> Ad Hominem is arguing with and/or attacking a person. Try not to respond to such attacks, except to politely expose the situation and redirect the discussion to the topic.
Threats are not allowed, whether towards people, groups or the community as a whole. Threats are disrespectful and utterly against TZM’s values.

**3. Respect the specific task of the activity.**

> Participate with the correct intentions for a certain project, task, assembly, content or anything in the movement. While the movement’s activity is inclusive for all, do not be a burden, distraction, disrespectful or problematic during that activity.

**4. Remember and uphold the Code of Conduct.**

> It is your responsibility and of everyone else to uphold and inform others participating within TZM our Code of Conduct to protect yourself, others and the integrity of TZM. It cannot be done alone and therefore everyone is required to do this.

**5. Make differences into strengths.**

> Instead of fixing all the things that you do not agree with another person, focus on resolving issues and learning from mistakes. Make the community larger by looking at your peers as collaborators rather than competitors.

## **CODE OF ETHICS & COMMUNITY**

The Zeitgeist Movement (TZM) practices egalitarian, transparent and equal opportunities for anyone wishing to contribute to the movement. When people claim association with TZM and commence working with other activists in TZM, they must comply with the Code of Conduct as instructed in this document. The aim is to interact in a community environment in association with TZM that does not hinder a person’s education, health, safety, and mental or physical development. Failure to comply with this Code of Conduct, especially in a community environment or when explicitly claiming association with TZM, may result in exclusion and denial of association with TZM as well as possible legal action if necessary.

## **HOW WE TREAT ONE ANOTHER**

Within TZM we treat each other with equal respect and dignity. This means that all persons are entitled to participate within the movement, while in a community environment free of harassment, bullying and discrimination.
Harassment, bullying and discrimination take many forms, including:
* Unwelcome remarks, gestures or physical contact 
* The display or circulation of offensive, derogatory or sexually explicit pictures or other materials
* Offensive or derogatory jokes or comments (explicit or by innuendo) 
* Verbal or physical abuse or threats
* Venting anger or frustration onto others

This movement is non-hierarchical and rejects any proposed solutions employing the use of violence or coercion. This is in order to obtain peace and harmony. If anybody claims the opposite, while also claiming association with TZM, they will be denied any association.

## **BEING INCLUSIVE**

We welcome and support people of all backgrounds and identities. This includes, but is not limited to persons of any sexual orientation, gender identity and expression, race, ethnicity, culture, national origin, socio-economic class, educational level, immigration status, age, size, family status, political belief, religion, and mental and physical ability are welcome to contribute to the movement.
There is no membership, tuition fee, academic background, specific credentials, pledge or anything aside from agreeing and following this Code of Conduct required to participate.
It is TZM’s strongest intention to protect the integrity, reputation and safety of all persons associated with the movement as well its own.

Everyone is coming from diverse backgrounds and experiences. We understand that people say and do things that may be unintentional. Despite these mistakes, we always understand to include them as part of a larger process. If you or any person is not capable of upholding TZM’s Code of Conduct at any given time, it is suggested to take a break and come back when you feel you will be able to better follow our Code of Conduct. If you are completely unable to act within the Code of Conduct, it is recommended that you disassociate from the movement.

## **DIVERSITY**

We can find strength in diversity. Different people have different perspectives on issues, and that can be valuable for solving problems or generating new ideas. Being unable to understand why someone holds a viewpoint, does not mean that they are wrong. We strive to remain open-minded to create a collaborative best solution, where multiple ideas are explored before selecting one. 

## **COMMUNITY ENVIRONMENT**

Any meeting location, community center, event, project space, website or other media, social media, instant messaging groups and any task of activism that is associated with TZM are defined as a community environment within the movement. All community environments associated with TZM are expected to comply to the standard of our Code of Conduct. It is also recommended that a copy of the Code of Conduct is available for all to see within the community environments. This includes ensuring the protection, integrity, physical safety, mental health and inclusion of all people present in a community environment of TZM.
Those who associate with TZM are all expected to follow the Code of Conduct and any additional safety rules during certain activities, while in a community environment. It is expected that the Code of Conduct will be enforced and reminded by everyone involved in TZM, as this is a communal responsibility.

If any community environment fails to fulfill the standards of the Code of Conduct and countermeasures are not being taken, then the community environment is to be aborted and will have no further association to TZM.

## **QUALITY STANDARDS OF CONTENT & ACTIVISM**

Anybody that is participating in TZM is welcome to create content or engage in supporting activities, while upholding the best possible integrity of TZM, and following a quality standard.
This standard applies to anything, in all forms, in any language or gesture rendered to represent TZM or any persons involved with it.

When participating in activities or creating content within the movement, the Code of Conduct of TZM must be obliged to. While there is no requirement of any previously acquired credentials to commence activity within TZM, there is a requirement to follow a quality standard that properly represents the movement and persons involved. For anyone wishing to participate and commence in TZM, while unsure of the basis of its activism and content are referred to read “The Zeitgeist Movement: Defined” book, which is found below under “Links & Resources”.
Some primary focuses of activity and content in TZM include philosophical, hypothesized and best practices of sustainability, science, technology, human rights, environmental protection and charity operations towards a supporting cause and the betterment of global society. Anything else that has not been listed from the following will be considered off-topic or not of strong relevance to TZM.

There are some areas that may not just be considered off-topic, but also harmful or misrepresenting of TZM and those associating with it. Falsification of TZM’s activity or values or any misleading information that claims to be in line with the movement will not be tolerated. This includes, but is not limited to: justifying the use of violence, coercion, manipulation, destruction or control against others to achieve goals; using or justifying the use of hate-speech or discrimination against any people and or other groups, political parties or nations; falsifying information for an agenda whether in support of or in disapproval of TZM; promoting political parties or campaigns in promotion of TZM; acting or registering TZM as a political entity; the use of misconception, assumptions or information without substantial evidence to support a claim.

Shall any person in association with TZM intentionally render any content or activity with any of these mentioned characteristics, their involvement is thereof excluded from the movement and will not be allowed to associate any further in any activities within TZM. Any parties that also claim no association with TZM and use any of the following characteristics to describe the motivations and ambitions of the movement will be refuted by a following statement and may face legal action.

Choose your words carefully, always conduct yourself professionally, be kind to others and do not hesitate to ask for help if you require assistance to do something of relevance for the movement.

## **FINANCIAL MATTERS**

TZM operates in a not for profit fashion and has no interests in any monetary gains. Any money that is collected in the name of TZM is solely used to finance or reimburse a cause of the movement’s activity. Any non-profit organization status, non-governmental organization status, bond, fund, bank account or any financial / legal organizational body that uses or is associated with TZM is expected to follow this. These financial / legal organizational bodies in association with TZM are refrained of creating profit for any person whether associated or unassociated with the movement. Shall this happen, the financial / legal organizational body, person in association or not unassociated with TZM will be excluded from the movement and will not be allowed to associate any further in any activities within TZM, their actions refuted by a following statement and may face legal action.

## **HAVE FUN!**

It is not always about reaching the end goal, but enjoying the journey along the way. All the other people you encounter, the things you learn and the steps and milestones you reach. Do not just let your activities and work for The Zeitgeist Movement be a chore or a tedious task that has to be done. Instead try to live a more sustainable, healthy, fulfilled life. Be kind to your neighbors and family, help your local community and promote a future for all without poverty and violence, whilst also seeing the bigger picture.

We look forward to seeing and meeting you and your supporting contributions for TZM and the rest of humanity. Be the change you wish to see in the world!

## **CONTACT**

The Zeitgeist Movement Global Website – https://www.thezeitgeistmovement.com

The Zeitgeist Movement Community Website- https://tzm.community/ 

## **LINKS & REFERENCES**

The Zeitgeist Movement: Defined – https://www.thezeitgeistmovement.com/wp-content/uploads/2017/10/The_Zeitgeist_Movement_Defined_PDF_Final.pdf

The Zeitgeist Movement: Activist Guide – https://tzm.community/activism/

#### **Version 1.0** Released 27th Nov 2020
