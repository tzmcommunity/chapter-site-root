---
layout: page
title: Siðferðisháttsemi
permalink: /code_of_conduct_is/
hide: true
---
[DEUTSCH](https://tzm.community/code_of_conduct_de/) ||| [ENGLISH](https://tzm.community/code_of_conduct/) ||| [SVENSKA](https://tzm.community/code_of_conduct_sv/)

## **KÆRU BORGARAR ÞESSA HEIMS**

Við bjóðum ykkur velkominn í Zeitgeist Hreyfinguna, alþjóðlega grasrótahreyfingu sem berst fyrir félagslegar breytingar á alheimsvísu til sjálfbærni. Þörf er á þáttöku þinni og allra annarra til þess að vera hluti af stærstu félagslegu breytingunni sem hefur nokkurn tímann verið komið af stað í sögu heimsins. Markmið hreyfingarinnar er að leiða í ljós nýtt sjónarhorn, nýjan hugsunargang ásamt því að koma á framfæri bestu þekkinguna á því hvernig við getum komið á aukinni sjálfbærni og lifað í sátt og samlyndi við hvort annað og alla náttúruna. Með því að taka þátt í hreyfingunni, áttu þátt í félagslegum samkomum, vísinda- og tæknirannsóknum og framkvæmdum, samfélagslegu átaki bæði heima fyrir og á alheimsvísu, styrktaraðgerðum, vernd og varðveitingu umhverfisins og jafnvel persónulegri þróun.
Sérhvert okkar ber persónulega ábyrgð á því að styðjast við okkar megingildi, sem þurfa að vera í samræmi við siðferðislegt ferli. Við höfum fært fram þessa Siðferðisháttsemi til þess að endurstaðhæfa langvarandi skuldbindingu okkar til þess að halda uppi þá ábyrgð og leiðbeina þáttöku þína og allra annarra í hreyfingunni.
Með okkar framferði, hjálpar Zeitgeist Hreyfingin til við að tryggja það að skilaboðið um sjáfbærara, samvinnuþýðara og innifaldara samhengi í samfélaginu heyrist. Skuldbinding til heilinda, heiðarlegra verka, og fylgdar með lögum er bráðnauðsynleg fyrir okkar áframhaldandi velgengni.

Takk fyrir þáttöku þína.

## **STUTT SAMANTEKT**

**1. Vinsamlegast komið fram við alla með virðingu.**

> Móðganir, niðrandi og/eða önnur vanvirðandi orðaskipti eru óviðeigandi og óásættanleg.

**2. Standið vinsamlegast ekki í persónuárásum.**

> Persónuárás þýðir að rífast við og/eða ráðast á persónuna í stað þess að flytja til máls umræðuefnið í því tilfelli sem snýst ekki um persónuna. Reynið að svara ekki slíkum árásum, nema til að fletta kurteisislega ofan af málinu og beina umræðunni á ný að efninu.
Ógnanir eru ekki leyfðar, hvort sem það sé gagnvart einstaklingum, hópum eða samfélaginu í heild. Ógnanir eru vanvirðandi og fara algjörlega þver á hugsanagang ZH.

**3. Sýnið virðingu gagnvart tilteknum verkum starfsins í hreyfingunni.*

> Takið með réttu ráði þátt í ákveðnu verki, verkefni, samkomu, efni eða öðru í hreyfingunni. Á meðan starf hreyfingarinnar er opið öllum, þá skuluð þið ekki valda öðrum óþægindum, truflunum, vanvirðingu né vandamálum í starfinu.

**4. Munið eftir og haldið Siðferðisháttseminni í heiðri.**

> Það er ykkar ábyrgð sem og allra að halda í heðri og fræða öðrum sem taka þátt í ZH um Siðferðisháttsemina til að vernda þig, aðra og heilindi ZH. Við getum það ekki ein og það þarf alla til.

**5. Breytin mismunum í styrkleika.**

> Í stað þess að lagfæra allt sem þú er ósammála öðrum, skaltu einbeita þér að því að leysa vandamál og læra af mistökum. Stækkaðu samfélagið með samvinnu í stað samkeppni.

## **SIÐFERÐISHÁTTSEMI OG SAMFÉLAGSLEGT**

Zeitgeist Hreyfingin (ZH) veitir öllum jafnréttismiðuðum, gegnsæum og jöfnum tækifærum til síns framlags til hreyfingarinnar. ZH hefur Aheimsmannréttindastaðal Sameinuðu Þjóðanna að grunni fyrir siðareglum í sínum samskiptum. Þegar einstaklingar mynda tengsl við ZH og hefja störf í tengslum við aðra einstaklinga, þá verða þeir að fara eftir Siðferðisháttseminni eins og leiðbeint er í þessu skjali. Markmiðið er að halda uppi gagnvirkum samskiptum í samfélagslegu umhverfi í tengslum við ZH sem hamlar ekki menntun, heilsu, öryggi, né andlegan eða líkamlegan þroska viðkomandi. Brot á þessari Siðferðisháttsemi, sérstaklega í samfélagslegu umhverfi eða þegar maður segist opinskátt tengjast ZH, gæti varðað brottrekstri úr og samskiptabanni í ZH ásamt mögulegri lögsókn ef þörf krefur.

## **HVERNIG VIÐ KOMUM FRAM VIÐ HVORT ANNAÐ**

Í ZH komum við fram við hvort annað með jafnri reisn og virðingu. Það þýðir að allir einstaklingar eiga rétt á að taka þátt í hreyfingunni undir samfélagslegu umhverfi sem laust er við áreitni, einelti og mismunun.
Áreitni, einelti og mismunun taka á sig ýmis form, eins og:
* Óboðlegar athugasemdir, látbrögð eða líkamssnertingar
* Birting eða dreifing á móðgandi, niðrandi eða kynferðislegum myndum eða öðrum hlutum
* Móðgandi eða niðrandi brandarar eða ummæli (bein eða óbein)
* Misnotkun eða ógnanir með orðum eða líkamlegum hætti
* Beina reiði eða gremju að öðrum

Þessi hreyfing er óstigskipt og hafnar hverjum lausnum fela í sér beiting ofbeldis eða þvingana. Það er gert til þess að ná fram sátt og samlyndi. Ef einhver í tengslum við ZH krefst hins gagnstæða við þá staðhæfingu, verður honum meinað allri þáttöku.

## **TAKIÐ ALLA MEÐ Í REIKNINGINN**

Við styðjum og bjóðum fólki af öllum bakgrunnum og sjálfsmyndum velkomið. Það innifelur, en takmarkast ekki við einstaklinga af nokkurri kynhneigð, kynvitund og kynjatjáningu, kynþátt, þjóðerni, menningu, þjóðaruppruna, þjóðfélags/-efnahagsstétt, litarhætti, innflytjendastöðu, kyni, aldri, stærð, fjölskyldustöðu, stjórnmálaskoðun, trú, sem og andlegri og líkamlegri getu er velkomið að veita hreyfingunni framlag.

Það þarf enga aðild, skólagjöld, menntabakgrunn, sérstök persónuskilríki, heit né neitt annað en að sammælast þessu Siðferðisháttsemi til að taka þátt. Sterkasti ásetningur ZH er í að vernda sín heilindi, mannorð og öryggi sem og allra í henni.
Allir hafa mismunandi bakgrunn, mismunandi erfiðleika og mismunandi sögur. Við skiljum að fólk getur sagt og gert hluti sem það ætlar sér ekki. 

Þrátt fyrir þessi mistök, sýnum við alltaf þann skilning að taka það með í reikninginn sem hluta af stærra ferli. Ef þú eða einhver getur ekki haldið Siðferðisháttsemi ZH í heiðri á tilteknum tíma, þá er mælt með því að taka sér frí og koma aftur á hentugri tíma til að taka þátt. Ef þú getur alls ekki fylgt eftir Siðferðisháttseminni þá verður að vísa þér frá hreyfingunni.

## **FJÖLBREYTNI**

Við sjáum styrkleika í fjölbreytni. Mismunandi fólk hefur mismunandi sjónarhorn á málum, og það getur reynst dýrmætt í að leysa vandamál og koma upp nýjum hugmyndum. Það að geta ekki skilið af hverju viðkomandi heldur uppi einu sjónarmiði þýðir ekki að hann hafi rangt fyrir sér. Ekki gleyma að við gerum öll mistök, og við komumst hvergi með því að kenna hvort öðrum um.
Einbeitið ykkur þess í stað að því að leysa vandamál, læra af mistökum og skilja ný sjórnarhorn.

## **SAMFÉLAGSLEGT UMHVERFI**

Sérhver mætingarstaður, samfélagsmiðstöð, viðburður, verkefnarými, vefsíða eða aðrir miðlar, samfélagsmiðlar, spjallhópar og sérhvert aðgerðaverk sem tengist ZH eru skilgreind sem samfélagslegt umhverfi innan hreyfingarinnar. Öll samfélagsleg umhverfi sem tengjast hreyfingunni eiga að fylgja eftir staðal Siðferðisháttsemi hreyfingarinnar. Einnig er mælt með því að afrit af Siðferðisháttseminni séu aðgengileg öllum til að sjá í samfélagslegu umhverfunum. Það felst í því að tryggja vernd, helindi, líkamlegt öryggi, andlega heilsu og þáttöku allra í samfélagslegu umhverfi ZH.

Allt fólk sem tengist ZH á að fylgja Siðferðisháttseminni ásamt frekari öryggisreglum og venjum í aðstöðu innan tiltekins samfélagsumhverfis. Allir eiga að framfylgja og minna aðra þáttakendur ZH á Siðferðisháttsemi hreyfingarinnar, sem og frekari öryggisráðstafanir, þar sem ábyrgðin hvílir ekki á sérstöku fólki á þátt í samfélagslega umhverfinu.
Ef eitthverju samfélagslegu umhverfi mistekst að uppfylla staðla Siðferðisháttseminnar og mótvægisaðgerðir eru ekki teknar þá verður að leysa því samfélagslegu umhverfi upp og mun ekki lengur tengjast ZH.

## **HÁGÆÐASTAÐALL FYRIR EFNI OG AÐGERÐASTEFNU**

Hver sem tekur þátt í aðgerðum ZH er velkomið að búa til efni eða taka þátt í verkum til stuðnings, með þeim ásetningi að halda uppi bestu mögulegu heilindi hreyfingarinnar ásamt öllu fólki sem lýsir yfir tengslum við hana auk þess að sammælast Siðferisháttsemi hreyfingarinnar. Þessi staðall á við um allt, í öllum formum, í öllu tali eða látbragði veitt til frammistöðu ZH eða fólks innan hennar.

Þegar þú tekur undir aðgerðum eða býrð til efni innan hreyfingarinnar og tengist ZH sem og öðru fólki innan hennar, þá samþykkir þú Siðferðisháttsemi ZH ásamt ábyrgðinni halda uppi og fræða aðra um meginreglu hennar. Á meðan engin þörf er á persónuskilríkjum sem maður hefur áður eignað sér til að hefja þáttöku innan ZH, þá þarf að fylgja á eftir hágæðastaðall sem stendur almennilega fyrir hreyfingunni og fólki innan hennar. Ef einhver vill byrja að taka þá í ZH en er ekki viss um grunninn að aðgerðastefnu og efni hennar verður honum bent á að lesa bókina "Zeitgeist Hreyfingin: Skilgreind (The Zeitgeist Movement Defined), sem er að finna fyrir neðan undir "Efni og Tenglar."

Nokkrar af helstu áherslum á aðgerðir og efni ZH fela í sér heimspeki, tilgátur og bestu athafnir á sviði sjálfbærni, vísinda, tækni, mannréttinda, umhverfisverndar og styrktaraðgerða fyrir málstað ásamt því að gera allt samfélagið í heiminum betra bæði fyrir allt mannkynið og náttúruna. Eitthvað allt annað en það ofangreinda verður tekið sem utan umræðuefnis eða mun ekki koma ZH sterklega við.

Einnig eru sum svið ekki einungis álitin veru utan umræðuefnis ZH, heldur líka skaðleg eða rangfærslur á hreyfingunni og þeim sem tengjast henni. Ekki verður leyfilegt að fikta í aðgerðum eða hugsanagang ZH, né koma fram villandi upplýsingum sem eru sagðar vera í samræmi við hreyfinguna. Það felur í sér en takmarkast ekki við: réttlætingu á beitingu ofbeldis, þvingunum, óheiðarlegum meðferðum, eyðileggingu eða stjórnun á hendur öðrum til að ná fram markmiðum; beitingu eða réttlætingu við að beita hatursáróðri gegn einhverju fólki eða öðrum hópum, stjórnmálaflokkum eða þjóðum; rangfærsla á upplýsingum til einhvers málaflokks hvort sem það sé til stuðnings við eða vanþóknunar gegnvart ZH; auglýsingu á stjórnmálaflokkum- eða herferðum til upphafningar á ZH; vinnu eða skráningu ZH sem stjórnmálaafl; notkun á ranghugmyndum, getgátum eða upplýsingum án haldbærra vísbendinga til að styðjast við ákveðna fullyrðingu.

Ef einhver aðili innan ZH skyldi koma fram með efni eða sýna af sér athæfi með einhverjum fyrrgreindum hætti, þá verður þáttöku hans útilokað úr hreyfingunni og má ekki lengur tengjast neinum aðgerðum innan ZH. Sérhverjir aðilar sem einnig kveðjast ekki eiga neinn þátt í ZH og beita sér í eihverjum áðurnefndum athöfnum til að lýsa markmiðum og metnaði hreyfingarinnar verður vísað á bug með eftirfarandi yfirlýsingu og gætu átt yfir höfði sér lögsókn.
Vandaðu orðaval þitt vel, hagaðu þér siðsamlega, komdu vel fram við aðra og ekki hika við að biðja um hjálp ef þig vantar aðstoð við eitthvað sem kemur hreyfingunni eitthvað við. 

## **FJÁRMÁL**

ZH starfar ekki í hagnaðarskyni og hefur engan áhuga á neinum peningagróða. Allur peningur sem safnast í nafni ZH verður eingöngu notað til að fjármagna eða bæta upp málstað í aðgerðum hreyfingarinnar. Sérhver sjálfseignarstofnunarstaða, frjáls félagasamtakastaða, skuldabréf, sjóður, bankareikningur eða nokkur fjárhags/lagaskipulagsstofnun í notkun hjá eða í tengslum við ZH á að fylgja því á eftir. Þessum fjárhags/lagaskipulagsstofnunum sem tengjast ZH verður forðað frá því að skapa gróða fyrir nokkurn mann hvort sem hann tengist hreyfingunni eða ekki. Ef það skyldi gerast þá verður fjárhags/lagaskipulagsstofnuninni ásamt einstaklingnum sem tengist eða tengist ekki ZH útilokað frá hreyfingunni og verður ekki lengur leyft að taka þátt í aðgerðum hennar. Einnig verður þá athöfnum þeirra vísað á bug með eftirfarandi yfirlýsingu og gætu á yfir höfði sér lögsókn.

## **SKEMMTIÐ YKKUR VEL!**

Þetta snýst ekki alltaf um að ná loka markmiðinu, heldur einnig að njótar ferðarinnar þangað í leiðinni, þ.e.a.s. alls hins fólksins sem þú hittir, hlutanna sem þú lærir ásamt skrefunum og áföngunum sem þú nærð. Láttu ekki bara aðgerðir þínar og starf þitt fyrir Zeitgeist Hreyfinguna vera eitthvert húsverk eða leiðinlegt starf sem þarf að sinna. Reyndu í staðinn að lifa sjálfbærra, heilbrigðara, fullnægðu lífi. Komdu vel fram við nágranna þína og fjölskyldu, hjálpaðu þínu nærsamfélagi og auglýstu framtíð fyrir alla án fátæktar og ofbeldis, á meðan þú sérð líka stærri heildarmyndina.

Við hlökkum til að sjá þig og þitt framlag til stuðnings við ZH og allt mannkynið. Vertu breytingin sem þú vilt sjá í heiminum!

## **SAMBAND**

The Zeitgeist Movement Global Website – https://www.thezeitgeistmovement.com

The Zeitgeist Movement Community Website- https://tzm.community/ 

## **TENGLAR & HEIMILDIR**

The Zeitgeist Movement: Defined – https://www.thezeitgeistmovement.com/wp-content/uploads/2017/10/The_Zeitgeist_Movement_Defined_PDF_Final.pdf

The Zeitgeist Movement: Activist Guide – https://tzm.community/activism/

#### **Útgáfa 1.0** Útgefinn 27th Nov 2020
