---
layout: roothome
bootstrap: true
postnav_title: "The Zeitgeist Movement Community Pages"
postnav_subtitle: "Learn more"
slim_header: true
postnav_link: "abouttzm"
postnav_linktext: "Learn more"
header_image: "/assets/img/autumn-219972_1280.jpg"

---
## Welcome to the TZM Community!

Here are news, projects, links and community websites we have gathered.

Check out all the latest news here [https://tzm.community/news](/news)

Other resources:

* [TZM Global Page](https://www.thezeitgeistmovement.com/)
* [TZM Discord Community](https://tzm.community/discord/)
* [TZM Facebook Page](https://www.facebook.com/tzmglobal/)
* [TZM Forum](https://tzm.one/)

