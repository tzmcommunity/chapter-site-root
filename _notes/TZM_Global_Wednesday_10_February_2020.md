---

layout: note

date: 10.02.2021

---

# **TZM meeting Wednesday December**

**EVERYBODY IS EXPECTED TO CO-CREATE THE NOTES.**

HOW TO hold meetings <https://board.net/p/New_Human_Right_Movement_Comm_Protocol>

**- Expected meeting duration: 1 hour**

Link to **all the MEETING NOTES** and template to create new ones:     <https://board.net/p/New_Human_Rights_Movement_assembly_motherpad>

## **AGENDA** (OPEN FOR EVERYONE TO ADD TO UNTIL MEETING STARTS ON THE HOUR)

(each item should have a conclusion added to summary after 20 minutes )

* Mark Enoch: The C sign
* Der Link: anouncements

## **ATTENDEES** (ADD YOUR NAME)

* Magni
* Mark Enoch
* Der Link
* Bradnon
* Alt-Universe
* Andy-D
* J. Thomas
* Alt-Universe

## **SPEAKERS CUE**  (EACH SPEAKER GETS 2 MIN)  add your name to the bottom of the list, strike through when you have spoken

* ~~Mark~~
* ~~Ajay~~
* ~~Denise~~
* ~~Mark~~
* ~~Andy~~

## **GENERAL NOTES** (minutes)

## **SUMMARY**

## 

## 

## **PROGRESS REPORTS** (SUMMARIZE YOUR RECENT WORK)

* Quinn Webb: Hello everyone! For anyone who has not gotten a chance to know me yet, I am from the Southeastern United States and I am a recent graduate from the University of Georgia with a Bachelor of Science degree in Environmental Economics (I'm a social science guy) so I decided to get involved with the survey team (now called the intelligence team) to help develop and improve information gathering pertinent to our movement. Currently, I have that work on the backburner for now because of the immense need to make changes to the TZM Wiki page. For those of you who have not yet had the displeasure of reading our wiki page, it is a mess to say the least. It has references to Michele Goldberg
* Mark Enoch: I have distributed a few hundred flyers in mailboxes and car windshields. I have share the invitation to the meeting in multiple groups and chats and social medias. I also have been inviting people one by one, that really helps bring people to the assemblies. I have been sharing chain messages promoting the transition to a shared world in the comments of almost every video I watch on youtube. I keep saying enjoy a shared world to everyone, instead of bye.
* Der Link: The end of the Discord server maintenance has been completed for first part of the year. We now have a bot-moderator to do basic management of the server, schedule reminders for meetings or give announcements. Its programming is not completely finished but is already full operation. Welcome Carl-bot :)
* Brandon: content stuff, being warm and kind to people
* J. Thomas: Onboarded with RBEU(RBE United) in the US. Am receiving guidance from Harakat and Gustavo within the organization as to the current needs. Currently, the organization is beginning to look at fundraising and is looking to build new features onto the existing website and Odoo platform.
* Magni+Boudika: We've been reading about **compassionate action** (mahayana) in out two Buddhist/Zen books, we've been reading about Australian **Aboriginal land use** and sustainability, and lastly we've read about different **permaculture centres**/communities. Yesterday we learned about gut health and how to boost **gut health** on a vegan diet (livet) which was very interesting! Last thing is that I (Magni) recieved a powerful **metaphor** which goes: that we are like little chickens, and there're the hatched and the unhatched, the unhatched are the harmful people who run on suffering and only have awareness as far as oneself's concerned, and the hatched are those who has gone beyond suffering and are driven by love and creates satisfaction.
* Andy D: No major activity in TZM UK chapter for the last year, with our z-day plans cancelled while the country was in lockdown. A few core members still hold some regular catch-up meetings in Discord. --- Personal work: Approaching the end of a 2-year-long participatory action research project looking at some waste plastic materials currently not recycled by a local municipal kerbside pickup, making them available as free craft materials to local artisans and getting feedback from that local creative community on how best to make use of these smaller waste streams, as the waste stream in question is normally too small for investment in centralised recycling machines. The aim being to turn a waste stream into a free resource, and expanding this with the help of a local charity. I will have much more to report on this once my thesis for this is ready for publication in a few months. --- Also continuing to work on establishing a tool library and hackerspace with my nearest Transition Town group, currently at the stage of a public consultation to advertise and assess demand. This was mothballed during lockdown, but we have plans for contact-free access to put in place to start up.
* Alt-Universe: Been telling people and inviting them to TZM.
* psyrave: I am collaborating with a fellow Zeitgeist movement member to create a strong Facebook page for TZM in Wisconsin. Also, my computer will be ready either this week or next to help with video collabs for Der Link and others

## **INDIVIDUAL TASKS to finish before next assembly** (ASSIGN NAME OF PERSON RESPONSIBLE)

*PERMANENT TASKS:*

*Producing next meeting (please specify which one):*

*+ Preparing notepad for next meeting and posting it with a call to the meeting in #news-and-updates:*

*Finishing the summary and posting these notes in #news-and-updates:*

\--------------

* Mark Enoch. Seek Norwegians to help TZM

## **GROUP TASKS for everyone to complete before next assembly**

* Consider visiting MFP UK, signing up and consider participating www.moneyfreeparty.uk
* Read the RBE united document 

## **HELP WANTED** (put your name)

* Ask for help for TZM Norway

## **LINKS**

## **CHAT and OTHER COMMENTS**

## 

## **Useful links**

<https://board.net/p/NHRM>[(TZM)_work_links](https://board.net/p/NHRM(TZM)_work_links)\
\
<https://www.moneyfreeparty.uk/social-therapy/>\
\
<https://anchor.fm/worldsummit/episodes/Interview-with-Douglas-Mallette---Cybernated-Farm-Systems-eng4k3>\
\
<https://www.youtube.com/channel/UCoCdl6NkVghMkN2Af8la0ew/videos>\
\
<https://www.youtube.com/channel/UCV-EEU6aLT7SSJegb453hOw>

<https://changingtheworldiseasy.com/diytp/>

## **INTRODUCTIONS/RESUMES OF ATTENDEES**

Enter here <https://docs.google.com/document/d/1d2Z-nor6lfUA7Ztso3hO0OF6UThqnd92eY979VT0VI8/edit#>