# Asamblea Abierta *Movimiento Zeitgeist* global \*2021 -\*enero_31

Guía orientativa sobre cómo realizar reuniones <https://board.net/p/Protocolo_de_Comunicacion_NMDH_>[(MZ](https://board.net/p/Protocolo_de_Comunicacion_NMDH_(MZ))

Las reuniones tienen una duración prevista de 1 hora.

Enlace a todas las Actas y a la plantilla para crear nuevas: <https://board.net/p/Tablamadre_Nuevo_Movimiento_de_Derechos_Humanos>

Por favor, participa en la redacción del acta.

**ORDEN DEL DÍA**

(Quienes hagan propuestas deben identificarse. Si quien hace una propuesta no está presente para explicarla, justificarla y argumentarla, puede que no la tratemos. Intentaremos llegar a una conclusión antes de 20 min).

==================================================================================

Mark Enoch: Transformar nuestras bibliotecas municipales en bancos de recursos\
\
Mark Enoch: Kotocoop

**ASISTENTES:** (Añádase, por favor).

Fonsi

Enrique Pampyn

eduardo (Chile)\
Georgios\
Mark Enoch\
Rasta\
Rydenr

Sabrina Nabu

Alejandro Tabius

**COLA PARA HABLAR**  (Si durante la reunión deseas hablar, añade tu nombre el último en esta cola. Tendrás unos 2 minutos para hacerlo. Sé breve y sintético y, cuando termines de hablar, tacha tu nombre. Antes de hablar asegúrate de que has pedido turno).

==================================================================================

**~~Mark~~\
Mark\
\
\
INFORMES DE PROGRESO**

Fonsi: He grabado los dos próximos vídeos para mi canal de youtube. Estoy deseando que vengan la primavera y verano para celebrar el Z-Day de 2020 que no se pudieron hacer debido a la pandemia. Hemos organizado un club de lectura para leer el libro 'El nuevo movimiento por los derechos humanos', de Peter Joseph en el que estamos 6 miembros y cada 15 días nos reunimos virtualmente para comentarlo.

Enrique: En el proceso de desmercantilización sigo donando ordenadores para personas que lo necesitan, especialmente alumnos de primaria, secundaria y superior a través de la ONG Junt@s no hay límites. Además sigo apoyando las campañas en favor de medidas que implican romper el mercado como la RMU\
\
Mark Enoch: Sigo trabajando con la biblioteca municipal para que se convierta en un Banco de Recursos. Estamos aprendiendo una nueva herramienta de decisión en grupo llamada nabú. Les digo a todos "disfruta de un mundo compartido" en lugar de "adiós". He cantado un montón de canciones que promueven un mundo compartido y el activismo a mis sobrinos. Ellos cantan con nosotros. He repartido tarjetas "anti-buisness" promoviendo el movimiento en las calles. He estado publicando llamadas a compartir el mundo que enseñan a la gente a promover el tren de pensamiento, a desmercantilizar y a guiar a la gente hacia aquí en casi todos los vídeos de youtube que veo.

CONCLUSIONES:

==================================================================================

TAREAS (Quien acepte una tarea, que ponga su nombre, la tarea que acepta y el plazo de tiempo en el que estima que lo hará)

==================================================================================

TAREAS FIJAS: Copiar el texto de convocatoria para la próxima reunión y difundirla en el canal #español_general de Discord, los grupos de chat del mZ y cualquier otro sitio web oficial.

Publicar las notas de esta reunión en #noticias (en la categoría Castellano):

\------------

**TAREAS de grupo** (escribe aquí que acción muy simple quieres que tod@s hagamos, ten en cuenta que también esperamos que en principio hagas todo lo que se pone aquí)\
\
\*Visitar y mirar un poco <https://linktr.ee/Fonsi> Todos mis rrss.

==================================================================================

**Se busca ayuda** (escribe aquí tu proyecto y como podemos contactar contigo para participar)

==================================================================================

**ENLACES** (Dejar una descripción clara del contexto en el que se ha tratado cada enlace).\
\
<https://changingtheworldiseasy.com/bancos-de-recursos/?lang=es>\
\
pagina para musicos, para ensayar virtualmene a distancia. Sala de ensayo virtual

<https://www.sagora.org/>\
\
<https://kotocoop.org/>\
\
<https://linktr.ee/Fonsi>\
\
Proyecto de aplicación de bibliotecas\
<https://cloud.tzm.community/index.php/s/iH6ToNoL4SGcm6S>

==================================================================================

**INTRODUCCIONES/CURRICULUM DE ASISTENTES**

Añádete por favor

<https://cloud.tzm.community/index.php/s/jyP97YT2XdFGoxz>

==================================================================================

Notas:

Guía orientativa sobre cómo realizar reuniones <https://board.net/p/Protocolo_de_Comunicacion_NMDH_>[(MZ](https://board.net/p/Protocolo_de_Comunicacion_NMDH_(MZ))

Las reuniones tienen una duración prevista de 1 hora.

Enlace a todas las Actas y a la plantilla para crear nuevas:<https://board.net/p/Tablamadre_Nuevo_Movimiento_de_Derechos_Humanos>