layout: note

date: 27.02.2021

---

# **TZM meeting Saturday February**

**EVERYBODY IS EXPECTED TO CO-CREATE THE NOTES.**

HOW TO hold meetings <https://board.net/p/New_Human_Right_Movement_Comm_Protocol>

**- Expected meeting duration: 1 hour**

Link to **all the MEETING NOTES** and template to create new ones:     <https://board.net/p/New_Human_Rights_Movement_assembly_motherpad>

## **AGENDA** (OPEN FOR EVERYONE TO ADD TO UNTIL MEETING STARTS ON THE HOUR)

(each item should have a conclusion added to summary after 20 minutes )

* [[Forum](https://tzm.one/)](<https://tzm.one/>) launched this month
* TZM Discord Server Updates

## **ATTENDEES** (ADD YOUR NAME)

* Magni
* Kees
* Der Link
* Mark Enoch
* Brandon
* Rowish
* Hobes
* Magni
* Bohdika
* Draiko III

## **SPEAKERS CUE**  (EACH SPEAKER GETS 2 MIN)  add your name to the bottom of the list, strike through when you have spoken

* Mark
* 
* 
* 
* 

## **GENERAL NOTES** (minutes)

## **SUMMARY**

soft launch of forum by Kees

## 

## **PROGRESS REPORTS** (SUMMARIZE YOUR RECENT WORK)

* Magni ᛘᛅᚴᚾᛁ : Making principles for living joyfully! Been pondering on "open-mindedness." I and Boudika have been thinking of restructuring the movement through making a focus-survey.
* Kees: launched the forum, working with Serf on a promotion plan for TZM NL, Serf will work on writing more for our TZM NL blog
* Mark Enoch: Continued to promote the train of thought in different groups online and in the comments of videos, inviting them to our sites and pages. I recently spoke to someone who saw my website written on the bathroom wall of a concert hall. He wants to join. I have been doing the C in pictures when friends take a picture of me, although I still sort of feel awkward
* Brandon - consolidating our approaches amongst the core team, video content
* 
* 
* 

## **INDIVIDUAL TASKS to finish before next assembly** (ASSIGN NAME OF PERSON RESPONSIBLE)

*PERMANENT TASKS:*

*Producing next meeting (please specify which one):*

*+ Preparing notepad for next meeting and posting it with a call to the meeting in #news-and-updates:*

*Finishing the summary and posting these notes in #news-and-updates:*

\--------------

* Mark Enoch. Seek Norwegians to help TZM

## **GROUP TASKS for everyone to complete before next assembly**

* Please join the new forum and write a comment somewhere. <https://tzm.one/>
* 

## **HELP WANTED** (put your name)

* Please make a video, write an article about some aspect of the train of thought. If that is too much for you,

## **LINKS**

<https://tzm.one/>\
<https://tzm.one/docs>\
\
<https://tzm.one/docs?topic=111>

<https://tzm.one/tags>\
<https://tzm.community/calendar>

## **CHAT and OTHER COMMENTS**

## 

## **Useful links**

<https://board.net/p/NHRM>[(TZM)_work_links](https://board.net/p/NHRM(TZM)_work_links)\
\
<https://www.moneyfreeparty.uk/social-therapy/>\
\
<https://anchor.fm/worldsummit/episodes/Interview-with-Douglas-Mallette---Cybernated-Farm-Systems-eng4k3>\
\
<https://www.youtube.com/channel/UCoCdl6NkVghMkN2Af8la0ew/videos>\
\
<https://www.youtube.com/channel/UCV-EEU6aLT7SSJegb453hOw>

<https://changingtheworldiseasy.com/diytp/>

## **INTRODUCTIONS/RESUMES OF ATTENDEES**

Enter here <https://docs.google.com/document/d/1d2Z-nor6lfUA7Ztso3hO0OF6UThqnd92eY979VT0VI8/edit#>