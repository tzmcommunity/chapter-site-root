Reunión de TZM el sábado de enero de 2021

SE ESPERA QUE TODO EL MUNDO COOPERE EN LA ELABORACIÓN DE LAS NOTAS.

CÓMO SE REALIZAN LAS REUNIONES <https://board.net/p/New>*Human*Right*Movement*Comm_Protocol

\- Duración prevista de la reunión: 1 hora

Enlace a todas las NOTAS DE LA REUNIÓN y plantilla para crear nuevas: <https://board.net/p/New>*Human*Rights*Movement*assembly_motherpad

ORDEN DEL DÍA (ABIERTO PARA QUE TODOS LO AÑADAN HASTA QUE LA REUNIÓN COMIENCE A LA HORA)

(cada punto debe tener una conclusión añadida al resumen después de 20 minutos )

**ASISTENTES (AÑADIR SU NOMBRE)**

Z O Tha Lone WOlf (Sean)

Mark Enochea

Kees

Der Link

Riku

Mecánico de medianoche

**INFORME DE PROGRESO (RESUMA SU TRABAJO RECIENTE)**

Nuestro informe de progreso se mantiene en nuestro blog, aquí está nuestro informe de progreso (TZM NL). Está en holandés, pero Google Translate hace un buen trabajo de traducción.

Mark Enoch: Sigo trabajando con la biblioteca municipal para que se convierta en un Banco de Recursos. También estoy organizando la asamblea de acceso abierto de mañana en español. Estamos aprendiendo una nueva herramienta de decisión en grupo llamada nabú. Les digo a todos "disfruta de un mundo compartido" en lugar de "adiós". He cantado un montón de canciones que promueven un mundo compartido y el activismo a mis sobrinos. Ellos cantan con nosotros. He repartido tarjetas "anti-buisness" promoviendo el movimiento en las calles. He estado publicando llamadas a compartir el mundo que enseñan a la gente a promover el tren de pensamiento, a desmercantilizar y a guiar a la gente hacia aquí en casi todos los vídeos de youtube que veo.

Kees: Actualización de la página web holandesa. Reunión de coiniciativa para abrir un nuevo foro en discourse.<https://forum.zeitgeistbeweging.nl/Created> una encuesta. <https://docs.google.com/forms/d/1>*JqHuaWyk3JJ1nNw17F7bzRXXMAPIB9FHWA*WFisjOI/We tener reuniones mensuales cada primer martes del mesMantener la página de facebook

Ajay hizo nuevas tarjetas de visita que anuncian TZM, TVP y el tren de pensamientos de la post escasez.

TZM Suecia ha traducido más trabajos del Código de Conducta a otros materiales en el idioma sueco. La Guía del Activista TZM también se publica en sueco hoy y se publica en el cumpleaños y la memoria de Björne Rova, un activista TZM de Suecia que falleció a finales de 2019 que también hizo la mayoría de las traducciones para TZM Suecia desde 2008 hasta 2019.

Las páginas de la comunidad TZM se están trabajando más en los próximos días y semanas.

Sean - He estado trabajando en algo de música nueva. Tengo una canción en particular que tiene un impacto a nivel de reunión. Poderosa. La compartiré con todos ustedes para que la escuchen, pero por lo demás estará en mi próximo lanzamiento. Tuve algunas buenas conversaciones con la gente.

Riku - Escribí y grabé un demo de una canción inspirada en Iron Maiden sobre el potencial humano y el caos y el orden en relación con eso. Ahora mismo estoy centrado en una banda, las grabaciones en el estudio podrían ocurrir mucho más tarde.

**TAREAS INDIVIDUALES a terminar antes de la próxima asamblea (ASIGNAR NOMBRE DE LA PERSONA RESPONSABLE)**\
\
Mark Enoch: Preparar la llamada a la acción

**TAREAS DE GRUPO para que todos completen antes de la próxima asamblea**

Por favor, rellena y pide a otros que rellenen <https://docs.google.com/forms/d/1>*JqHuaWyk3JJ1nNw17F7bzRXXMAPIB9FHWA*WFisjOI/

**SE BUSCA AYUDA (pon tu nombre)**

Se necesitan más hablantes de noruego para ayudar ( TZM Noruega)<https://www.facebook.com/tzmnorway>

**ENLACES**

<https://openaccesseconomy.org/doku.php?id=resource_banks>

<https://cloud.tzm.community/index.php/login>

<https://forum.zeitgeistbeweging.nl/https://www.youtube.com/watch?v=GvU0xlmIpt0>

Enlaces útiles

<https://board.net/p/NHRM>(TZM)*enlaces*de_trabajo

PRESENTACIONES/CURRÍCULOS DE LOS ASISTENTES

Introduzca aquí <https://docs.google.com/document/d/1d2Z-nor6lfUA7Ztso3hO0OF6UThqnd92eY979VT0VI8/edit#>

Traducción realizada con la versión gratuita del traductor www.DeepL.com/Translator