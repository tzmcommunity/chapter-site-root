Asamblea Abierta Movimiento Zeitgeist global 2021_Febrero__28

Guía orientativa sobre cómo realizar reuniones <https://board.net/p/Protocolo_de_Comunicacion_NMDH_>[(MZ](https://board.net/p/Protocolo_de_Comunicacion_NMDH_(MZ))

Las reuniones tienen una duración prevista de 1 hora.

Enlace a todas las Actas y a la plantilla para crear nuevas: <https://board.net/p/Tablamadre_Nuevo_Movimiento_de_Derechos_Humanos>

Por favor, participa en la redacción del acta.

**ORDEN DEL DÍA**

(Quienes hagan propuestas deben identificarse. Si quien hace una propuesta no está presente para explicarla, justificarla y argumentarla, puede que no la tratemos. Intentaremos llegar a una conclusión antes de 20 mins).

==================================================================================

Alfonso: \
Actualizar los acuerdos básicos (AABB) consensuados en 2011. Hilo del foro donde se trató:\
<https://movimientozeitgeist.com/forum/hilo/propuesta-actualizada-de-los-acuerdos-basicos-aabb/>

AABB consensuados: [https://docs.google.com/document/d/1cX>](%5Bhttps://docs.google.com/document/d/1cX%3EompHdR5nD5al76lhY2s0TAyxlt7GUogLo1nEv9g/edit?usp=sharing)*[ompHdR5nD5a](%5Bhttps://docs.google.com/document/d/1cX%3EompHdR5nD5al76lhY2s0TAyxlt7GUogLo1nEv9g/edit?usp=sharing)*[l76lhY2s0TAyxlt7GUogLo1nEv9g/edit?usp=sharing](%5Bhttps://docs.google.com/document/d/1cX%3EompHdR5nD5al76lhY2s0TAyxlt7GUogLo1nEv9g/edit?usp=sharing)

Nueva propuesta actualizada: <https://docs.google.com/document/d/1qfJlxWhIyy03gk6MlwjX0DyurJiJit6UmywXANCYRyM/edit?usp=sharing>

Propuestas de acciones:

1. II Encuentro virtual: definir temas
2. Organizar proyecto de Banco de recursos común para presentar en ayuntamientos
3. Organizar videoforums sobre diversos temas

**ASISTENTES:** (Añádase, por favor).

==================================================================================

Mark Enoch

Alfonso\
Enrique

**COLA PARA HABLAR**  (Si durante la reunión deseas hablar, añade tu nombre el último en esta cola. Tendrás unos 2 minutos para hacerlo. Sé breve y sintético y, cuando termines de hablar, tacha tu nombre. Antes de hablar asegúrate de que has pedido turno).

==================================================================================

**INFORMES DE PROGRESO**

==================================================================================

Mark Enoch: Hicimos una asamblea abierta global en inglès. Invité gente de forma directa. Eso funciona para que se animan a venir. Di 500$ de donación a ShareBay. He hecho la C para un mundo compartido en varias fotos cuando amigos me tomaban foto. He pegado un texto de difusión de nuestra mirada con enlaçes a nuestras pàginas como comentario en casi todos los videos que he visto en youtube. 

Enrique: Respecto de la sesión anterior no ha habido cambios. Sigo con la donación de ordenadores a alumnos de primaria y secundaria, aunque también a otras personas que lo requieren.

Alfonso: Seguimos reuniéndonos cada 15 días unas 6 personas en el club de lectura para leer y comentar el libro 'El nuevo movimiento de los derechos humanos', de Peter Joseph.\
Me gusta publicar varios tuits y publicaciones en face cargadas de hashtags e ideas condensadas. Publico poco, pero de calidad.

**CONCLUSIONES:**

=====================================================================================================\
\
QUedamos el domingo 7 a las 19 con Sabrina

**TAREAS** (Quien acepte una tarea, que ponga su nombre, la tarea que acepta y el plazo de tiempo en el que estima que lo hará)

=====================================================================================================

TAREAS FIJAS: Copiar el texto de convocatoria para la próxima reunión y difundirla en el canal #español_general de Discord, los grupos de chat del mZ y cualquier otro sitio web oficial.

Publicar las notas de esta reunión en #noticias (en la categoria Castellano):

\------------\
Mark Enoch: Añadir estos puntos en primeros en el siguiente orden del dia. Buscar documento de banco de recursos y compartirlo con Enrique. 

Mark Enoch: Invitar a Sabrina a la reunion de domingo

Enrique Pampyn: Elaborar una propuesta de Banco de recursos a partir de los documentos de Marc

**TAREAS de grupo** (escribe aqui que acción muy simple quieres que tod@s hagamos, ten en cuenta que tambien esperamos que a hagas todo lo que se pone aquí)

==================================================================================

1. Acabar de votar la propuesta en Nabú: <http://nabu.pt/default.html?grupo=TZM-ES&idioma=es>
2. Difundir el tweet de Fonsi
3. Invitar a otros a venir a le asamblea, en privado ademàs que en grupo. 

**Se busca ayuda** (escribe aqui tu proyecto y como podemos contactar contigo para participar)

=====================================================================================================

**ENLACES** (Dejar una descripción clara del contexto en el que se ha tratado cada enlace).

Por ejemplo: El compañero Sergio nos comparte un vídeo sobre el activismo en su ciudad: ENLACE.

=====================================================================================================

**INTRODUCCIONES/CURRICULUM DE ASISTENTES**

Añádete por favor

<https://docs.google.com/document/d/1d2Z-nor6lfUA7Ztso3hO0OF6UThqnd92eY979VT0VI8/edit#>

=====================================================================================================

Notas:

Guía orientativa sobre cómo realizar reuniones <https://board.net/p/Protocolo_de_Comunicacion_NMDH_>[(MZ](https://board.net/p/Protocolo_de_Comunicacion_NMDH_(MZ))

Las reuniones tienen una duración prevista de 1 hora.

Enlace a todas las Actas y a la plantilla para crear nuevas:<https://board.net/p/Tablamadre_Nuevo_Movimiento_de_Derechos_Humanos>