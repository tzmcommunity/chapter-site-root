# Asamblea Abierta Movimiento Zeitgeistglobal 2020 diciembre 27

\
Guía orientativa sobre cómo realizar reuniones <https://board.net/p/Protocolo_de_Comunicacion_NMDH_>[(MZ)](https://board.net/p/Protocolo_de_Comunicacion_NMDH_(MZ))

Las reuniones tienen una duración prevista de 1 hora.

Enlace a todas las Actas y a la plantilla para crear nuevas: <https://board.net/p/Tablamadre_Nuevo_Movimiento_de_Derechos_Humanos>

Por favor, participa en la redacción del acta.

**ORDEN DEL DÍA**

(Quienes hagan propuestas deben identificarse. Si quien hace una propuesta no está presente para explicarla, justificarla y argumentarla, puede que no la tratemos. Intentaremos llegar a una conclusión antes de 20 mins).\
\
\*Mark Enoch: Importancia de las asambleas abiertas y como hacer crecer la actividad MZ\
\
\+ Sabrina: nabu para tomar decisiones grupales <http://nabu.pt/web/default.html>

==================================================================================

**ASISTENTES:** (Añádase, por favor).\
\
Mark Enoch\
Sabrina\
Tabius

==================================================================================

**COLA PARA HABLAR**  (Si durante la reunión deseas hablar, añade tu nombre el último en esta cola. Tendrás unos 2 minutos para hacerlo. Sé breve y sintético y, cuando termines de hablar, tacha tu nombre. Antes de hablar asegúrate de que has pedido turno).

==================================================================================

**INFORMES DE PROGRESO**

==================================================================================\*Mark Enoch: He abierto la parada gratiferia para la Navidad Sostenible de TZM. Abro al azar cuando tengo una hora libre. Mi sobrina ayudó a montarlo y a colorear los carteles. También he estado copiando la "Llamada a compartir el mundo" en la sección de comentarios de todos los videos que veo. El capítulo español tuvo una charla de 3 personas en línea el domingo pasado. Era sobre la necesidad de cambio. Tuvimos alrededor de 19 oyentes en vivo. Hicieron preguntas. Duró 1h 30m en total. Luego la compartí en unos 8 grupos de medios sociales diferentes. Ha recibido 64 visitas hasta ahora. <https://youtu.be/addrTVmUBK4> . Me he acostumbrado a decir "Feliz mundo compartido" en vez de "adiós" y lo hago siempre.

\*Informes de la asamblea abierta anglohablante traducidas automáticamente:

> \
> \
> \*Mark Enoch: He abierto el stand realmente libre para la Navidad Sostenible de TZM. Abro al azar cuando tengo una hora libre. Mi sobrina ayudó a montarlo y a colorear los carteles. También he estado copiando el "Llamado a compartir el mundo" en la sección de comentarios de todos los videos que veo. El capítulo español tuvo una charla de 3 personas en línea el domingo pasado. Era sobre la necesidad de cambio. Tuvimos alrededor de 19 oyentes en vivo. Hicieron preguntas. Duró 1h 30m en total. Luego la compartí en unos 8 grupos de medios sociales diferentes. Ha recibido 64 visitas hasta ahora. <https://youtu.be/addrTVmUBK4> . Me he acostumbrado a decir "Disfruta de un mundo compartido" en vez de "adiós" y lo hago todo el tiempo.
>
> \*Thaidaree: cosas personales pasando. Además, Irlanda es probable que entre en el tercer encierro, así que no hay mucho que hacer en términos de activismo. Estaré trabajando en el API para los proyectos: <https://github.com/TZMCommunity/tzm-projects>
>
> MidnightMechanic: ha estado transmitiendo y promoviendo procesos de pensamiento sostenible a unas 5 personas por transmisión, podría utilizar más espectadores para abordar temas más serios
>
> Sean Z O Tha Lobo Solitario: He tenido algunas charlas interesantes con la gente, incluyendo un caso con una señora que tiene una estrecha conexión cultural con los comités. Después de la conversación, un par de días más tarde, cuando la volví a irritar, dijo que había entendido el amor de todas las partes y dijo que había presentado algunas ideas a la junta.
>
> Kees: En relación con esta iniciativa, tengo las siguientes actualizaciones:
>
> Hice contacto con varios grupos de medios sociales para difundir la iniciativa y recibí algunos comentarios. La mayoría son positivas. En cuanto a los comentarios menos positivos, la preocupación es sobre todo acerca de la vida del proyecto. A algunas personas les preocupa que pueda desaparecer. Otras no tienen objeciones duras y han recibido una útil retroalimentación positiva para mejorar la iniciativa.
>
> Se puso en contacto con Michael Kubler sobre el proyecto. Establecí un espacio de trabajo en el Gitlab, porque la infraestructura de la comunidad tzm.community carece en algunas áreas. Esto podría, por ejemplo, poner a tzm.community en las listas negras de spam. El Gitlab será un lugar para trabajar en la infraestructura de TI y compartir conocimientos y tareas para mejorarla. Otros planes de mejora se enumeran allí también en términos de seguridad. El libro de jugadas de Ansible para el servidor de Discurso será colocado allí también, incluyendo documentación sobre la recuperación de desastres, actualización, etc. Si la gente con experiencia profesional en Linux está dispuesta a ayudar. Por favor, envíeme su nombre de usuario de Gitlab y lo agregaré: k.dejong@zeitgeistbeweging.nl
>
> Programará una reunión en enero para discutirlo de forma más interactiva, después de lo cual comenzará la encuesta.
>
> Mientras tanto, estoy esperando que Cliff publique la URL de la iniciativa en la página global de TZM FB.

CONCLUSIONES:

==================================================================================

**TAREAS** (Quien acepte una tarea, que ponga su nombre, la tarea que acepta y el plazo de tiempo en el que estima que lo hará)

==================================================================================

TAREAS FIJAS: Copiar el texto de convocatoria para la próxima reunión y difundirla en el canal #español_general de Discord, los grupos de chat del mZ y cualquier otro sitio web oficial.

Publicar las notas de esta reunión en #noticias (en la categoria Castellano):

\------------

**TAREAS de grupo** (escribe aqui que acción muy simple quieres que tod@s hagamos, ten en cuenta que tambien esperamos que a hagas todo lo que se pone aquí)\
\
Aprovechad el tiempo de las fiestas para invitar a conocidos a ver contenido entretenido que enseña la mirada critica al ánimo de lucro como Interreflections o El Dilema de las Redes o la serie Mr.Robot. 

==================================================================================

**Se busca ayuda** (escribe aqui tu proyecto y como podemos contactar contigo para participar)

==================================================================================

**ENLACES** (Dejar una descripción clara del contexto en el que se ha tratado cada enlace).

Por ejemplo: El compañero Sergio nos comparte un vídeo sobre el activismo en su ciudad: ENLACE.\
\
<http://nabu.pt/web/default.html>\
<http://nabu.pt/>

==================================================================================

**INTRODUCCIONES/CURRICULUM DE ASISTENTES**

Añádete por favor

<https://docs.google.com/document/d/1d2Z-nor6lfUA7Ztso3hO0OF6UThqnd92eY979VT0VI8/edit#>

==================================================================================

Notas:

Guía orientativa sobre cómo realizar reuniones <https://board.net/p/Protocolo_de_Comunicacion_NMDH_>[(MZ)](https://board.net/p/Protocolo_de_Comunicacion_NMDH_(MZ))

Las reuniones tienen una duración prevista de 1 hora.

Enlace a todas las Actas y a la plantilla para crear nuevas:<https://board.net/p/Tablamadre_Nuevo_Movimiento_de_Derechos_Humanos>